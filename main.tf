terraform {
  required_version = "~> 1.5.2"
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 5.15.0"
    }
  }
}

provider "aws" {
  region = "us-east-1"
}


######################################
# EC2 Instance
######################################

resource "aws_key_pair" "example_key" {
  key_name   = var.key_name
  public_key = var.public_key
}

resource "aws_instance" "example_instance" {
  ami                         = var.ami
  associate_public_ip_address = var.associate_public_ip_address
  instance_type               = var.instance_type
  key_name                    = aws_key_pair.example_key.key_name
  vpc_security_group_ids      = var.vpc_security_group_ids
  subnet_id                   = var.subnet_id
  tags                        = var.tags
}


######################################
# OUTPUTS
######################################

output "ec2_public_ip" {
  value = aws_instance.example_instance.public_ip
}

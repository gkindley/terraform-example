###############################
# Key Pair Variables
###############################

variable "key_name" {
  description = "The name of the key pair to use for the instance"
  type        = string
}

variable "public_key" {
  description = "The public key to use for the instance"
  type        = string
}

###############################
# EC2 Variables
###############################

variable "ami" {
  description = "The AMI to use for the instance"
  type        = string
}

variable "associate_public_ip_address" {
  description = "Associate a public IP address with the instance"
  type        = bool
  default     = false
}

variable "instance_type" {
  description = "The type of instance to start"
  type        = string
  default     = "t2.nano"
}

variable "vpc_security_group_ids" {
  description = "A list of security group IDs to associate with"
  type        = list(string)
}

variable "subnet_id" {
  description = "The VPC Subnet ID to launch in"
  type        = string
}

variable "tags" {
  description = "A mapping of tags to assign to the resource"
  type        = map(string)
  default = {
    Name = "example-instance"
  }
}
